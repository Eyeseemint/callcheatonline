﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvaNet.Services
{
    public class ResourcePathResolverService
    {
        public string WebRootPath { get; set; }
    }
}
