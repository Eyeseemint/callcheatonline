﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AvaNet.Models.ViewModels.ForumViewModels
{
    public class PinnedForumThreadsEditViewModel
    {
        public ForumTopic ForumTopic { get; set; }

        public PinnedForumThreads PinnedForumThreads { get; set; }
    }
}
