﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaccomStrike.Library.Data.ViewModel
{
    public class CreateTaccomStrikeUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
