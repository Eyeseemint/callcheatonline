namespace TaccomStrike.Library.Data.ViewModel
{
    public class CreateUserLogin
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}