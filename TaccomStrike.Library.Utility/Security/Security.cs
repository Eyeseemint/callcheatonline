﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaccomStrike.Library.Utility.Security
{
    public static class Security
    {
        public static string AuthenticationScheme = "TaccomStrikeAuthentication";
        public static string CookieName = "TaccomStrikeCookie";
    }
}
